import React from 'react';
class HatsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hats: []
        };
        this.handleOnClick = this.handleOnClick.bind(this)
    }
    async handleOnClick(event) {
        const hatUrl = `http://localhost:8090/api/hats/${event}`
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
            const response2 = await fetch("http://localhost:8090/api/hats/")
            if (response2.ok) {
                const data = await response2.json()
                this.setState({ 'hats': data.hats })
            }
        }
    }
    async componentDidMount() {
        const hatsUrl = "http://localhost:8090/api/hats/";
        const hatsResponse = await fetch(hatsUrl)
        if (hatsResponse.ok) {
            const data = await hatsResponse.json()
            this.setState({ 'hats': data.hats })
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        return (
            <div className="container-fluid p-4 my-1">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Fabric</th>
                            <th scope="col">Style Name</th>
                            <th scope="col">Color</th>
                            <th scope="col">Closet Name</th>
                            <th scope="col">Picture</th>
                            <th scope="col">Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.hats.map((hat) => {
                            return (
                                <tr key={hat.id}>
                                    <td>{hat.fabric}</td>
                                    <td>{hat.style_name}</td>
                                    <td>{hat.color}</td>
                                    <td>{hat.location.closet_name}</td>
                                    <td>
                                        <img
                                            src={hat.picture_url}
                                            width={150}
                                            alt='pic img'
                                        />
                                    </td>
                                    {/* <td>
                                        <button className="btn btn-secondary" onClick={() => this.handleDetails(hat.id)}>
                                            DETAILS</button>
                                    </td> */}
                                    <td>
                                        <button className="btn btn-danger" onClick={() => this.handleOnClick(hat.id)}>
                                            DELETE</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default HatsList;
