import React from 'react';

class ListLocations extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            locations: []
        }
        this.handleOnClick = this.handleOnClick.bind(this)
    }
    async handleOnClick(event) {
        const locationsUrl = `http://localhost:8100/api/locations/${event}`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(locationsUrl, fetchConfig)
        if (response.ok) {
            const response2 = await fetch('http://localhost:8100/api/locations/')
            if (response2.ok) {
                const data = await response2.json()
                this.setState({ "locations": data.locations })
            }
        }
    }
    async componentDidMount() {
        const locationsurl = "http://localhost:8100/api/locations/"
        const locationsResponse = await fetch(locationsurl)
        if (locationsResponse.ok) {
            const data = await locationsResponse.json()
            this.setState({ 'locations': data.locations })
        }
    }
    render() {
        return (
            <div className="container-fluid p-4 my-1">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Closet Name</th>
                            <th scope="col">Section Number</th>
                            <th scope="col">Shelf Number</th>
                            <th scope="col">Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.locations.map((location) => {
                            return (
                                <tr key={location.id}>
                                    <td>{location.closet_name}</td>
                                    <td>{location.section_number}</td>
                                    <td>{location.shelf_number}</td>
                                    <td>
                                        <button className="btn btn-danger" onClick={() => this.handleOnClick(location.id)}>
                                            DELETE</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}
export default ListLocations;
