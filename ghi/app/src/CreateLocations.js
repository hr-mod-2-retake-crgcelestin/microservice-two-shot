import React from 'react';
class CreateLocations extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            closet_name: '',
            section_number: '',
            shelf_number: '',
            hasLocationCreated: false,
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        });
    }
    async handleSubmit(event) {
        event.preventDefault()
        const data = { ...this.state }
        delete data.CreateLocations
        delete data.hasLocationCreated
        const LocationUrl = `http://localhost:8100/api/locations/`
        const datajson = JSON.stringify(data)
        const fetchConfig = {
            method: 'post',
            body: datajson,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(LocationUrl, fetchConfig)
        if (response.ok) {
            const cleared = {
                closet_name: '',
                section_number: '',
                shelf_number: ' ',
                hasLocationCreated: true,
            }
            this.setState(cleared)
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        let successAlert = "alert alert-success d-none mb-0"
        let formClass = '';
        if (this.state.hasLocationCreated) {
            successAlert = "alert alert-success mb-0"
            formClass = 'd-none';
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a New Location</h1>
                        <form
                            className={formClass}
                            onSubmit={this.handleSubmit}
                            id="create-location-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Location Name"
                                    required type="text"
                                    id="closet_name"
                                    name="closet_name"
                                    className="form-control"
                                    value={this.state.closet_name} />
                                <label htmlFor="closet_name">Location Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Section Number"
                                    required type="text"
                                    name="section_number"
                                    id="section_number"
                                    className="form-control"
                                    value={this.state.section_number} />
                                <label htmlFor="section_number">Section Number</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Shelf Number"
                                    required type="text"
                                    name="shelf_number"
                                    id="shelf_number"
                                    className="form-control"
                                    value={this.state.shelf_number} />
                                <label htmlFor="shelf_number">Shelf Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div
                            className={successAlert}
                            id="success-message">
                            Congratulations! You Created a Location
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CreateLocations;
