import React from 'react'


class ShoesList extends React.Component {
    constructor() {
        super()
        this.state = {
            "shoes": []
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8080/api/shoes/"
        let response = await fetch(url)

        if (response.ok) {
            let data = await response.json()
            this.setState({ "shoes": data.shoes })
        }
    }

    async delete(id) {

        const url = `http://localhost:8080/api/shoes/${id}`
        const fetchConfig = {
            method: 'delete',
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            this.componentDidMount()
        }
    }

    render() {
        return (
            <div className="container-fluid p-4 my-1">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Shoe Model</th>
                            <th>Shoe Bin</th>
                            <th>Shoe Color</th>
                            <th>Picture</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.shoes.map((shoe) => {
                            return (
                                <tr key={shoe.id}>
                                    <td> {shoe.model_name} </td>
                                    <td> {shoe.bin} </td>
                                    <td> {shoe.color} </td>
                                    <td> <img width={100} src={shoe.picture_url} className="img-thumbnail tile  " alt="Responsive image" /> </td>
                                    <td>
                                        <button className="btn btn-danger"
                                            value={shoe.id}
                                            onClick={() => this.delete(shoe.id)}
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ShoesList;
