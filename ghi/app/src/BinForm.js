import React from 'react';
class CreateBins extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            closet_name: '',
            bin_number: '',
            bin_size: '',
            hasBinCreated: false,
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        });
    }
    async handleSubmit(event) {
        event.preventDefault()
        const data = { ...this.state }
        delete data.CreateBins
        delete data.hasBinCreated
        const BinUrl = `http://localhost:8100/api/bins/`
        const datajson = JSON.stringify(data)
        const fetchConfig = {
            method: 'post',
            body: datajson,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(BinUrl, fetchConfig)
        if (response.ok) {
            const cleared = {
                closet_name: '',
                bin_number: '',
                bin_size: ' ',
                hasBinCreated: true,
            }
            this.setState(cleared)
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        let successAlert = "alert alert-success d-none mb-0"
        let formClass = '';
        if (this.state.hasBinCreated) {
            successAlert = "alert alert-success mb-0"
            formClass = 'd-none';
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create A New Bin</h1>
                        <form
                            className={formClass}
                            onSubmit={this.handleSubmit}
                            id="create-bin-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Bin Name"
                                    required type="text"
                                    id="closet_name"
                                    name="closet_name"
                                    className="form-control"
                                    value={this.state.closet_name} />
                                <label htmlFor="closet_name">Bin Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Bin Number"
                                    required type="text"
                                    name="bin_number"
                                    id="bin_number"
                                    className="form-control"
                                    value={this.state.bin_number} />
                                <label htmlFor="bin_number">Bin Number</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Bin Size"
                                    required type="text"
                                    name="bin_size"
                                    id="bin_size"
                                    className="form-control"
                                    value={this.state.bin_size} />
                                <label htmlFor="bin_size">Bin Size</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div
                            className={successAlert}
                            id="success-message">
                            Congratulations! You Created a Bin
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CreateBins;
