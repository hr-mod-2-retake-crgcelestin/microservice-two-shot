import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import CreateHats from './CreateHats';
import ListLocations from './ListLocations';
import CreateLocations from './CreateLocations';
import ShoesList from './ShoesList';
import CreateShoe from './CreateShoe';
import CreateBins from './BinForm';
import BinList from './BinList';

function App(props) {

  if (props.shoes === undefined) {
    return null
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="new" element={<CreateHats />} />
            <Route path="list" element={<HatsList />} >
            </Route>
          </Route>
          <Route path="locations">
            <Route path="list" element={<ListLocations />} />
            <Route path="new" element={<CreateLocations />} />
          </Route>
          <Route path="shoes">
            < Route path="list" element={<ShoesList />} />
            <Route path="new" element={<CreateShoe />} />
          </Route>
          <Route path="bins">
            <Route path='list' element=
              {<BinList />} />
            <Route path='new' element=
              {<CreateBins />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
