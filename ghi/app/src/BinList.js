import React from 'react';

class BinList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            bins: []
        }
        this.handleOnClick = this.handleOnClick.bind(this)
    }
    async handleOnClick(event) {
        const binsUrl = `http://localhost:8100/api/bins/${event}`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(binsUrl, fetchConfig)
        if (response.ok) {
            const response2 = await fetch('http://localhost:8100/api/bins/')
            if (response2.ok) {
                const data = await response2.json()
                this.setState({ "bins": data.bins })
            }
        }
    }
    async componentDidMount() {
        const binsUrl = "http://localhost:8100/api/bins/"
        const binsResponse = await fetch(binsUrl)
        if (binsResponse.ok) {
            const data = await binsResponse.json()
            this.setState({ 'bins': data.bins })
        }
    }
    render() {
        return (
            <div className="container-fluid p-4 my-1">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Closet Name</th>
                            <th scope="col">Bin Number</th>
                            <th scope="col">Bin Size</th>
                            <th scope="col">Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.bins.map((bin) => {
                            return (
                                <tr key={bin.id}>
                                    <td>{bin.closet_name}</td>
                                    <td>{bin.bin_number}</td>
                                    <td>{bin.bin_size}</td>
                                    <td>
                                        <button className="btn btn-danger" onClick={() => this.handleOnClick(bin.id)}>
                                            DELETE</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}
export default BinList;
