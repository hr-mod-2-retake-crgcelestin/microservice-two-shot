import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-info">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Wardrobify</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/shoes/list/">Shoes List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/hats/list/">Hats List</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/locations/list/"> Locations List</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/bins/list/"> Bins List</NavLink>
            </li>
          </ul>
          <div className="col-xs-3">
            <button className="btn-block">
              <NavLink className="nav-link" to="/hats/new/">Create Hat</NavLink>
            </button>
            <button className="btn-block">
              <NavLink className="nav-link" to="/locations/new/">Create Location</NavLink>
            </button >
            <button className="btn-block">
              <NavLink className="nav-link" to="/shoes/new">Create a Shoe</NavLink>
            </button>
            <button className="btn-block">
              <NavLink className="nav-link" to="/bins/new">Create a Bin</NavLink>
            </button>
          </div>
        </div>
      </div>
    </nav >
  )
}

export default Nav;
