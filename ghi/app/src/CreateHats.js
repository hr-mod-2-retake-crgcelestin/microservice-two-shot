import React from 'react';
class CreateHats extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            locations: [],
            fabric: "",
            style_name: "",
            color: "",
            picture_url: "",
            hasHatCreated: false,
        };
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name;
        this.setState({
            [name]: value
        });
    }
    async handleSubmit(event) {
        event.preventDefault()
        const data = { ...this.state }
        delete data.locations
        delete data.hasHatCreated
        const HatsUrl = 'http://localhost:8090/api/hats/';
        const datajson = JSON.stringify(data)
        const fetchConfig = {
            method: 'post',
            body: datajson,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(HatsUrl, fetchConfig)
        if (response.ok) {
            const newHat = await response.json()
            const cleared = {
                location: "",
                fabric: "",
                style_name: "",
                color: "",
                picture_url: "",
                hasHatCreated: true,
            };
            this.setState(cleared)
        } else {
            throw new Error('response is not ok')
        }
    }
    async componentDidMount() {
        const LvoUrl = "http://localhost:8100/api/locations/"
        const LvoResponse = await fetch(LvoUrl)
        if (LvoResponse.ok) {
            const data = await LvoResponse.json()
            this.setState({ 'locations': data.locations })
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        let successAlert = "alert alert-success d-none mb-0"
        let formClass = '';
        if (this.state.hasHatCreated) {
            successAlert = "alert alert-success mb-0"
            formClass = 'd-none';
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create A New Hat</h1>
                        <form
                            className={formClass}
                            onSubmit={this.handleSubmit}
                            id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Hat Fabric"
                                    required type="text"
                                    id="fabric"
                                    name="fabric"
                                    className="form-control"
                                    value={this.state.fabric} />
                                <label htmlFor="fabric">Hat Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Hat Style"
                                    required type="text"
                                    name="style_name"
                                    id="style_name"
                                    className="form-control"
                                    value={this.state.style_name} />
                                <label htmlFor="style_name">Style Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Color"
                                    required type="text"
                                    name="color"
                                    id="color"
                                    className="form-control"
                                    value={this.state.color} />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="https://example.com"
                                    required type="url"
                                    name="picture_url"
                                    id="picture_url"
                                    className="form-control"
                                    value={this.state.picture_url} />
                                <label htmlFor="picture_url">Hat Image Url</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={this.handleInputChange}
                                    name="location"
                                    id="location"
                                    className="form-select"
                                    value={this.state.location}
                                    required>
                                    <option value="">Choose a Location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.href} value={location.href}>
                                                {location.closet_name}
                                            </option>
                                        )
                                    })};
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div
                            className={successAlert}
                            id="success-message">
                            Congratulations! You Created a Hat
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default CreateHats;
