from django.contrib import admin
from django.urls import path

from .views import api_list_shoes, api_detail_shoes

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:id>/",api_detail_shoes, name="api_detail_shoes")
]
