from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import BinVO, Shoes
import json


class BinVODetailEncoder (ModelEncoder):
    model = BinVO
    properties = ["name", "import_href"]


class ShoeListEncoder (ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "id",
        "picture_url",
        "color",
        ]

    def get_extra_data(self, o):
        return {"bin": o.bin.name}


class ShoeDetailEncoder (ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "manufacturer",
        "color",
        "picture_url",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href = bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_shoes(request,id):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=id)
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse ({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Shoes.objects.filter(id=id).update(**content)
        shoes = Shoes.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder= ShoeDetailEncoder,
            safe=False
        )
