import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import LocationVO, Hat
from common.json import ModelEncoder
# Create your views here.

class LocationVOListEncoder(ModelEncoder):
    model=LocationVO
    properties=["import_href", "closet_name"]

class LocationVODetailEncoder(ModelEncoder):
    model=LocationVO
    properties=["import_href", "closet_name", "section_number", "shelf_number"]

class HatListEncoder(ModelEncoder):
    model=Hat
    properties= ["id","fabric", "style_name", "color", "location", "picture_url"]
    encoders={
        "location":LocationVOListEncoder(),
    }

class HatDetailEncoder(ModelEncoder):
    model=Hat
    properties= [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders={
        "location":LocationVODetailEncoder(),
    }

@require_http_methods(["GET","POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method=="GET":
        if location_vo_id is not None:
            Hats=Hat.objects.filter(id=location_vo_id)
        else:
            Hats=Hat.objects.all()
        return JsonResponse(
            {"hats":Hats},
            encoder=HatListEncoder,
        )
    else:
        content=json.loads(request.body)
        try:
            location_href=content["location"]
            location=LocationVO.objects.get(import_href=location_href)
            content["location"]=location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"alert":"location does not exist"},
                status=400
            )
        hat=Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )

@require_http_methods(["GET","PUT", "DELETE"])
def api_show_hats(request,pk):
    if request.method=="GET":
        hats=Hat.objects.get(pk=pk)
        return JsonResponse(
            {"hat":hats},
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method=="DELETE":
        count, _=Hat.objects.filter(pk=pk).delete()
        return JsonResponse(
            {"hat is deleted":count>0}
        )
    else:
        content=json.loads(request.body)
        Hat.objects.filter(pk=pk).update(**content)
        hats=Hat.objects.get(pk=pk)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False
        )
