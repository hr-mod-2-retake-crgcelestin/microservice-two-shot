# Wardrobify

Team:

* Nate Bradley - Shoes Microservice
* Craig Celestin - Hats Microservice

## Step-by-step instructions
    GitLab
        Follow link to gitlab page.
            https://gitlab.com/hr-mod-2-retake-crgcelestin/microservice-two-shot
        Fork the project.
        Open terminal and cd into desired directory.
        Copy and paste the https under the blue clone drop down on gitlab.
            git clone $<paste the copied https here>
        Open project.
            open .
    Open Docker desktop - build and start up the containers in the app directory in terminal.
        docker-compose build
        docker-compose up
    Open insomnia and create GET, POST, PUT, DELETE requests for bins/locations. Below are shortcuts to url paths and json body inputs.
    In the webapp, the Nav bar has links: Home, Shoes, Hats, Add a Shoe, Add a Hat.
        Home - home page
        Shoes - List view of created shoes
        Hats - List view of created hats
        Add a shoe - Redirects to a create shoe form.
        Add a hat - Redirects to a create hat form.
    Filling out either form will create data for a new shoe or hat which will then be listed in the shoes or hats nav links.


    *** INSOMNIA ***

                        --- BINS ---

        GET LIST BINS   | url = http://localhost:8100/api/bins/
        POST BIN        | url = http://localhost:8100/api/bins/
                        | json body = {
                                    "closet_name": " ",
                                    "bin_number": " ",
                                    "bin_size": " "
                                }
        PUT BIN         | url = http://localhost:8100//api/bins/<id>/
                        | *json body same as above*
        DELETE BIN      | url = http://localhost:8100//api/bins/<id>/

                        --- SHOES ---

        GET LIST SHOES          | url = http://localhost:8080/api/shoes
        GET DETAIL SHOES        | url = http://localhost:8080/api/shoes/<id>
        POST SHOE               | url = http://localhost:8080/api/shoes/
                                | json body = {
                                            "manufacturer": " ",
                                            "model_name": " ",
                                            "color": " ",
                                            "picture_url": " ",
                                            "bin": " "
                                        }

        PUT SHOE                | url = http://localhost:8080/api/shoes/<id>/
        DELETE SHOE             | url = http://localhost:8080/api/shoes/<id>/


                    --LOCATIONS--
        GET LIST LOCATIONS | (GET) url=http://localhost:8100/api/locations/
             exepected response:  {
	                "locations": [ ...]
                    }
        POST LOCATIONS     | (POST) url=http://localhost:8100/api/locations/
                           | json body={
                               "closet_name": " ",
                               "section_number": " ",
                               "shelf_number": " "
                           }
                           example:
                           {
                            "closet_name":"closet 1",
                            "section_number":"1",
                            "shelf_number":"1"
                           }
                           Expected response data:
                           {
                                "href": "/api/locations/1/",
                                "id": 3,
                                "closet_name": "closet 1",
                                "section_number": "1",
                                "shelf_number": "1"
                            }
        GET LIST DETAILS  |  (GET) http://localhost:8100/api/locations/<int:id>/
            example response:
                {
                    "href": "/api/locations/2/",
                    "id": 2,
                    "closet_name": "closet 2",
                    "section_number": 2,
                    "shelf_number": 1
                }
        PUT LOCATION      |  (PUT) http://localhost:8100/api/locations/<int:id>/
                            *same as post json*
        DELETE LOCATION   |  (DELETE) http://localhost:8100/api/locations/<imt:id>
                        {
                    "message": "Does not exist"
                        }
                    --HATS--
        GET LIST HATS    | (GET) http://localhost:8090/api/hats/
        expected response:
            {
	        "hats": [
                ...
            ]
            }
        POST HAT         |
       (POST) http://localhost:8090/api/hats/
                          JSON body={
                            {
                            "fabric": "fabric1",
                            "style_name":"style1",
                            "color":"color1",
                            "picture_url": "https://pbs.twimg.com/media/E6rKNztVIAERYZT?format=jpg&name=large",
                            "location":"/api/locations/<int:id>/"
                          }
                        expected response:
                        {
                            "id": 16,
                            "fabric": "fabric1",
                            "style_name": "style1",
                            "color": "color1",
                            "location": {
                                "import_href": "/api/locations/1/",
                                "closet_name": "closet 2"
                            },
                            "picture_url": "https://pbs.twimg.com/media/E6rKNztVIAERYZT?format=jpg&name=large"
                        }
        GET HAT DETAILS | (GET) http://localhost:8090/api/hats/<int:id>/
        expected response:
                    {
                "hat": [
                    {
                        "id": 10,
                        "fabric": "fabric1",
                        "style_name": "style1",
                        "color": "color1",
                        "picture_url": "https://pbs.twimg.com/ext_tw_video_thumb/1427456739822116888/pu/img/UigPM2Ut-QTaEOCM.jpg",
                        "location": {
                            "import_href": "/api/locations/1/",
                            "closet_name": "closet 1",
                            "section_number": 1,
                            "shelf_number": 2
                        }
                    }
                ]
            }
        PUT HAT         | (PUT) http://localhost:8090/api/hats/<int:id>/
                        *same as post json*
        DELETE HAT      | (DELETE)http://localhost:8090/api/hats/<int:id>/
        expected response:
                    {
                        "hat is deleted": true
                    }
## Design

![](images/diagram.png)

## Shoes microservice

The shoes microservice was created with RESTful methods to provide frontend capabilities to create, view shoes, and delete methods. Shoes has a BinVo and Shoes model.
The shoes microservice includes a poller though BinVO model which includes a href to the specific shoe.

## Hats microservice

The Hats Microservice allows the user to :
    View a Hats List
    Create a Hat
    View Locations List
    Create a Location
The models in the hats microservice include:
    Hats model: representation of a hat with attributes of fabric, style name, color, picture url, and location
    LocationVO Model: representation of getting a location from wardrobe api with attributes of the import_href, closet name, section number, and shelf number

The Hats MS is in charge of allowing one to view hats in the wardrboe, creating a new hat, and (bonus) create a new location and view locations for one to easily understand and add hats. The Hats MS poller get data from wardrobe MS in order to receive Locations data and would allow a user to correctly id where said item is in said location and with said location view details of its specific closet name, section, and shelf number.
